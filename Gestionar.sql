-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 21-06-2020 a las 23:38:20
-- Versión del servidor: 5.7.30-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `Gestionar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `IdActividad` smallint(5) NOT NULL,
  `IdTipoActividad` smallint(5) DEFAULT NULL,
  `TituloActividad` varchar(30) DEFAULT NULL,
  `Actividad` text,
  `FechaEmision` datetime DEFAULT NULL,
  `FechaEntrega` datetime DEFAULT NULL,
  `Nota` text,
  `IdEstadoActividad` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anuncio`
--

CREATE TABLE `anuncio` (
  `IdAnuncio` smallint(5) NOT NULL,
  `IdTipoAnuncio` smallint(5) DEFAULT NULL,
  `Anuncio` text,
  `IdRol` smallint(5) DEFAULT NULL,
  `FechaEmision` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciclolectivo`
--

CREATE TABLE `ciclolectivo` (
  `IdCicloLectivo` smallint(5) NOT NULL,
  `CicloLectivo` year(4) DEFAULT NULL,
  `IdPeriodo` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciclomateria`
--

CREATE TABLE `ciclomateria` (
  `IdCicloMateria` smallint(5) NOT NULL,
  `IdPeriodo` smallint(5) NOT NULL,
  `IdEspecialidad` smallint(5) NOT NULL,
  `IdCurso` smallint(5) NOT NULL,
  `IdDivision` smallint(5) NOT NULL,
  `IdTurno` smallint(5) NOT NULL,
  `IdMateria` smallint(5) NOT NULL,
  `IdClase` smallint(5) NOT NULL,
  `Id_Rol` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clase`
--

CREATE TABLE `clase` (
  `IdClase` smallint(5) NOT NULL,
  `IdRol` smallint(5) DEFAULT NULL,
  `IdRegistro` smallint(5) DEFAULT NULL,
  `IdTema` smallint(5) DEFAULT NULL,
  `IdActividad` smallint(5) DEFAULT NULL,
  `IdAnuncio` smallint(5) DEFAULT NULL,
  `Id_CicloMateria` smallint(5) DEFAULT NULL,
  `FechaClase` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigopostal`
--

CREATE TABLE `codigopostal` (
  `IdCodigoPostal` smallint(5) NOT NULL,
  `CodigoPostal` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `IdCurso` smallint(5) NOT NULL,
  `Curso` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `division`
--

CREATE TABLE `division` (
  `IdDivision` smallint(5) NOT NULL,
  `Division` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dni`
--

CREATE TABLE `dni` (
  `IdDni` smallint(5) NOT NULL,
  `Dni` int(10) DEFAULT NULL,
  `IdTipoDni` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio`
--

CREATE TABLE `domicilio` (
  `IdDomicilio` smallint(5) NOT NULL,
  `Calle` varchar(100) DEFAULT NULL,
  `Numero` int(10) DEFAULT NULL,
  `Piso` smallint(3) DEFAULT NULL,
  `Depto` varchar(10) DEFAULT NULL,
  `IdEntreCalles` smallint(5) DEFAULT NULL,
  `IdLocalidad` smallint(5) DEFAULT NULL,
  `Referencia` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrecalles`
--

CREATE TABLE `entrecalles` (
  `IdEntrecalles` smallint(5) NOT NULL,
  `Entrecalle1` varchar(10) DEFAULT NULL,
  `Entrecalle2` varchar(10) DEFAULT NULL,
  `IdDomicilio` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especialidad`
--

CREATE TABLE `especialidad` (
  `IdEspecialidad` smallint(5) NOT NULL,
  `Especialidad` varchar(100) DEFAULT NULL,
  `IdCurso` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadoactividad`
--

CREATE TABLE `estadoactividad` (
  `IdEstadoActividad` smallint(5) NOT NULL,
  `EstadoActividad` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadoalumno`
--

CREATE TABLE `estadoalumno` (
  `IdEstadoAlumno` smallint(5) NOT NULL,
  `EstadoAlumno` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `IdGenero` smallint(5) NOT NULL,
  `Genero` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `institucion`
--

CREATE TABLE `institucion` (
  `IdInstitucion` smallint(5) NOT NULL,
  `CUE` varchar(30) DEFAULT NULL,
  `NombreInstitucion` varchar(40) DEFAULT NULL,
  `IdDomicilio` smallint(5) DEFAULT NULL,
  `IdNivel` smallint(5) DEFAULT NULL,
  `IdTelefono` smallint(5) DEFAULT NULL,
  `EmailInstitucion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

CREATE TABLE `localidad` (
  `IdLocalidad` smallint(5) NOT NULL,
  `Localidad` varchar(50) DEFAULT NULL,
  `IdPartido` smallint(5) DEFAULT NULL,
  `IdCodigoPostal` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE `materia` (
  `IdMateria` smallint(5) NOT NULL,
  `Materia` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel`
--

CREATE TABLE `nivel` (
  `IdNivel` smallint(5) NOT NULL,
  `Nivel` varchar(35) DEFAULT NULL,
  `IdEspecialidad` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `IdPais` smallint(5) NOT NULL,
  `Pais` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partido`
--

CREATE TABLE `partido` (
  `IdPartido` smallint(5) NOT NULL,
  `Partido` varchar(50) NOT NULL,
  `IdProvincia` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodo`
--

CREATE TABLE `periodo` (
  `IdPeriodo` smallint(5) NOT NULL,
  `Periodo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `IdPersona` smallint(5) NOT NULL,
  `NumeroLegajo` varchar(30) DEFAULT NULL,
  `Nombres` varchar(100) DEFAULT NULL,
  `Apellido` varchar(100) DEFAULT NULL,
  `IdDni` smallint(5) DEFAULT NULL,
  `IdDomicilio` smallint(5) DEFAULT NULL,
  `IdGenero` smallint(5) DEFAULT NULL,
  `IdTelefono` smallint(5) DEFAULT NULL,
  `EmailPersona` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `IdProvincia` smallint(5) NOT NULL,
  `Provincia` varchar(50) DEFAULT NULL,
  `IdPais` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro`
--

CREATE TABLE `registro` (
  `IdRegistro` smallint(5) NOT NULL,
  `FechaRegistro` datetime DEFAULT NULL,
  `IdRol` smallint(5) DEFAULT NULL,
  `IdEstadoAlumno` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `IdRol` smallint(5) NOT NULL,
  `Rol` varchar(20) DEFAULT NULL,
  `IdPersona` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono`
--

CREATE TABLE `telefono` (
  `IdTelefono` smallint(5) NOT NULL,
  `Telefono` varchar(100) DEFAULT NULL,
  `IdTipoTelefono` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tema`
--

CREATE TABLE `tema` (
  `IdTema` smallint(5) NOT NULL,
  `Tema` varchar(300) DEFAULT NULL,
  `IdClase` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoactividad`
--

CREATE TABLE `tipoactividad` (
  `IdTipoActividad` smallint(5) NOT NULL,
  `TipoActividad` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoanuncio`
--

CREATE TABLE `tipoanuncio` (
  `IdTipoAnuncio` smallint(5) NOT NULL,
  `TipoAnuncio` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodni`
--

CREATE TABLE `tipodni` (
  `IdTipoDni` smallint(5) NOT NULL,
  `TipoDni` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipotelefono`
--

CREATE TABLE `tipotelefono` (
  `IdTipoTelefono` smallint(5) NOT NULL,
  `TipoTelefono` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

CREATE TABLE `turno` (
  `IdTurno` smallint(5) NOT NULL,
  `Turno` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`IdActividad`),
  ADD KEY `IdTipoActividad` (`IdTipoActividad`,`IdEstadoActividad`),
  ADD KEY `IdEstadoActividad` (`IdEstadoActividad`);

--
-- Indices de la tabla `anuncio`
--
ALTER TABLE `anuncio`
  ADD PRIMARY KEY (`IdAnuncio`),
  ADD KEY `IdTipoAnuncio` (`IdTipoAnuncio`,`IdRol`),
  ADD KEY `IdRol` (`IdRol`);

--
-- Indices de la tabla `ciclolectivo`
--
ALTER TABLE `ciclolectivo`
  ADD PRIMARY KEY (`IdCicloLectivo`),
  ADD KEY `IdPeriodo` (`IdPeriodo`);

--
-- Indices de la tabla `ciclomateria`
--
ALTER TABLE `ciclomateria`
  ADD PRIMARY KEY (`IdCicloMateria`),
  ADD KEY `IdCurso` (`IdCurso`),
  ADD KEY `IdDivision` (`IdDivision`),
  ADD KEY `IdTurno` (`IdTurno`),
  ADD KEY `IdMateria` (`IdMateria`),
  ADD KEY `IdPeriodo` (`IdPeriodo`,`IdCurso`,`IdDivision`,`IdTurno`,`IdMateria`,`Id_Rol`),
  ADD KEY `IdEspecialidad` (`IdEspecialidad`),
  ADD KEY `Id_Rol` (`Id_Rol`),
  ADD KEY `IdClase` (`IdClase`);

--
-- Indices de la tabla `clase`
--
ALTER TABLE `clase`
  ADD PRIMARY KEY (`IdClase`),
  ADD KEY `IdRol` (`IdRol`,`IdRegistro`,`IdTema`,`IdActividad`,`IdAnuncio`,`Id_CicloMateria`),
  ADD KEY `Id_CicloMateria` (`Id_CicloMateria`),
  ADD KEY `IdTema` (`IdTema`),
  ADD KEY `IdActividad` (`IdActividad`),
  ADD KEY `IdRegistro` (`IdRegistro`),
  ADD KEY `clase_ibfk_1` (`IdAnuncio`);

--
-- Indices de la tabla `codigopostal`
--
ALTER TABLE `codigopostal`
  ADD PRIMARY KEY (`IdCodigoPostal`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`IdCurso`);

--
-- Indices de la tabla `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`IdDivision`);

--
-- Indices de la tabla `dni`
--
ALTER TABLE `dni`
  ADD PRIMARY KEY (`IdDni`),
  ADD KEY `IdTipoDni` (`IdTipoDni`);

--
-- Indices de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`IdDomicilio`),
  ADD KEY `IdEntreCalles` (`IdEntreCalles`,`IdLocalidad`),
  ADD KEY `IdLocalidad` (`IdLocalidad`);

--
-- Indices de la tabla `entrecalles`
--
ALTER TABLE `entrecalles`
  ADD PRIMARY KEY (`IdEntrecalles`),
  ADD KEY `IdDomicilio` (`IdDomicilio`);

--
-- Indices de la tabla `especialidad`
--
ALTER TABLE `especialidad`
  ADD PRIMARY KEY (`IdEspecialidad`),
  ADD KEY `IdCurso` (`IdCurso`);

--
-- Indices de la tabla `estadoactividad`
--
ALTER TABLE `estadoactividad`
  ADD PRIMARY KEY (`IdEstadoActividad`);

--
-- Indices de la tabla `estadoalumno`
--
ALTER TABLE `estadoalumno`
  ADD PRIMARY KEY (`IdEstadoAlumno`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`IdGenero`);

--
-- Indices de la tabla `institucion`
--
ALTER TABLE `institucion`
  ADD PRIMARY KEY (`IdInstitucion`),
  ADD KEY `IdDomicilio` (`IdDomicilio`,`IdNivel`,`IdTelefono`),
  ADD KEY `IdTelefono` (`IdTelefono`),
  ADD KEY `IdNivel` (`IdNivel`);

--
-- Indices de la tabla `localidad`
--
ALTER TABLE `localidad`
  ADD PRIMARY KEY (`IdLocalidad`),
  ADD KEY `IdProvincia` (`IdPartido`,`IdCodigoPostal`),
  ADD KEY `IdCodigoPostal` (`IdCodigoPostal`);

--
-- Indices de la tabla `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`IdMateria`);

--
-- Indices de la tabla `nivel`
--
ALTER TABLE `nivel`
  ADD PRIMARY KEY (`IdNivel`),
  ADD KEY `Id_Especialidad` (`IdEspecialidad`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`IdPais`);

--
-- Indices de la tabla `partido`
--
ALTER TABLE `partido`
  ADD PRIMARY KEY (`IdPartido`),
  ADD KEY `IdProvincia` (`IdProvincia`);

--
-- Indices de la tabla `periodo`
--
ALTER TABLE `periodo`
  ADD PRIMARY KEY (`IdPeriodo`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`IdPersona`),
  ADD KEY `IdDni` (`IdDni`,`IdDomicilio`,`IdGenero`,`IdTelefono`),
  ADD KEY `IdTelefono` (`IdTelefono`),
  ADD KEY `IdDomicilio` (`IdDomicilio`),
  ADD KEY `IdGenero` (`IdGenero`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`IdProvincia`),
  ADD KEY `IdPais` (`IdPais`);

--
-- Indices de la tabla `registro`
--
ALTER TABLE `registro`
  ADD PRIMARY KEY (`IdRegistro`),
  ADD KEY `IdRol` (`IdRol`,`IdEstadoAlumno`),
  ADD KEY `IdEstadoAlumno` (`IdEstadoAlumno`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`IdRol`),
  ADD KEY `IdPersona` (`IdPersona`);

--
-- Indices de la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD PRIMARY KEY (`IdTelefono`),
  ADD KEY `IdTipoTelefono` (`IdTipoTelefono`);

--
-- Indices de la tabla `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`IdTema`),
  ADD KEY `IdClase` (`IdClase`);

--
-- Indices de la tabla `tipoactividad`
--
ALTER TABLE `tipoactividad`
  ADD PRIMARY KEY (`IdTipoActividad`);

--
-- Indices de la tabla `tipoanuncio`
--
ALTER TABLE `tipoanuncio`
  ADD PRIMARY KEY (`IdTipoAnuncio`);

--
-- Indices de la tabla `tipodni`
--
ALTER TABLE `tipodni`
  ADD PRIMARY KEY (`IdTipoDni`);

--
-- Indices de la tabla `tipotelefono`
--
ALTER TABLE `tipotelefono`
  ADD PRIMARY KEY (`IdTipoTelefono`);

--
-- Indices de la tabla `turno`
--
ALTER TABLE `turno`
  ADD PRIMARY KEY (`IdTurno`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividad`
--
ALTER TABLE `actividad`
  MODIFY `IdActividad` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `anuncio`
--
ALTER TABLE `anuncio`
  MODIFY `IdAnuncio` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ciclolectivo`
--
ALTER TABLE `ciclolectivo`
  MODIFY `IdCicloLectivo` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ciclomateria`
--
ALTER TABLE `ciclomateria`
  MODIFY `IdCicloMateria` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `clase`
--
ALTER TABLE `clase`
  MODIFY `IdClase` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `codigopostal`
--
ALTER TABLE `codigopostal`
  MODIFY `IdCodigoPostal` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `curso`
--
ALTER TABLE `curso`
  MODIFY `IdCurso` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `division`
--
ALTER TABLE `division`
  MODIFY `IdDivision` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `dni`
--
ALTER TABLE `dni`
  MODIFY `IdDni` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  MODIFY `IdDomicilio` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `entrecalles`
--
ALTER TABLE `entrecalles`
  MODIFY `IdEntrecalles` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `especialidad`
--
ALTER TABLE `especialidad`
  MODIFY `IdEspecialidad` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estadoalumno`
--
ALTER TABLE `estadoalumno`
  MODIFY `IdEstadoAlumno` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `genero`
--
ALTER TABLE `genero`
  MODIFY `IdGenero` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `institucion`
--
ALTER TABLE `institucion`
  MODIFY `IdInstitucion` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `localidad`
--
ALTER TABLE `localidad`
  MODIFY `IdLocalidad` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `materia`
--
ALTER TABLE `materia`
  MODIFY `IdMateria` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `IdPais` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `partido`
--
ALTER TABLE `partido`
  MODIFY `IdPartido` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `periodo`
--
ALTER TABLE `periodo`
  MODIFY `IdPeriodo` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `IdPersona` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `provincia`
--
ALTER TABLE `provincia`
  MODIFY `IdProvincia` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `registro`
--
ALTER TABLE `registro`
  MODIFY `IdRegistro` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `IdRol` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `telefono`
--
ALTER TABLE `telefono`
  MODIFY `IdTelefono` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tema`
--
ALTER TABLE `tema`
  MODIFY `IdTema` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipoanuncio`
--
ALTER TABLE `tipoanuncio`
  MODIFY `IdTipoAnuncio` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipodni`
--
ALTER TABLE `tipodni`
  MODIFY `IdTipoDni` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tipotelefono`
--
ALTER TABLE `tipotelefono`
  MODIFY `IdTipoTelefono` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `turno`
--
ALTER TABLE `turno`
  MODIFY `IdTurno` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `actividad_ibfk_1` FOREIGN KEY (`IdEstadoActividad`) REFERENCES `estadoactividad` (`IdEstadoActividad`),
  ADD CONSTRAINT `actividad_ibfk_2` FOREIGN KEY (`IdTipoActividad`) REFERENCES `tipoactividad` (`IdTipoActividad`);

--
-- Filtros para la tabla `anuncio`
--
ALTER TABLE `anuncio`
  ADD CONSTRAINT `anuncio_ibfk_1` FOREIGN KEY (`IdTipoAnuncio`) REFERENCES `tipoanuncio` (`IdTipoAnuncio`),
  ADD CONSTRAINT `anuncio_ibfk_2` FOREIGN KEY (`IdRol`) REFERENCES `rol` (`IdRol`);

--
-- Filtros para la tabla `ciclolectivo`
--
ALTER TABLE `ciclolectivo`
  ADD CONSTRAINT `ciclolectivo_ibfk_2` FOREIGN KEY (`IdPeriodo`) REFERENCES `periodo` (`IdPeriodo`);

--
-- Filtros para la tabla `ciclomateria`
--
ALTER TABLE `ciclomateria`
  ADD CONSTRAINT `ciclomateria_ibfk_1` FOREIGN KEY (`IdPeriodo`) REFERENCES `periodo` (`IdPeriodo`),
  ADD CONSTRAINT `ciclomateria_ibfk_2` FOREIGN KEY (`IdEspecialidad`) REFERENCES `especialidad` (`IdEspecialidad`),
  ADD CONSTRAINT `ciclomateria_ibfk_3` FOREIGN KEY (`IdCurso`) REFERENCES `curso` (`IdCurso`),
  ADD CONSTRAINT `ciclomateria_ibfk_4` FOREIGN KEY (`IdDivision`) REFERENCES `division` (`IdDivision`),
  ADD CONSTRAINT `ciclomateria_ibfk_5` FOREIGN KEY (`IdTurno`) REFERENCES `turno` (`IdTurno`),
  ADD CONSTRAINT `ciclomateria_ibfk_6` FOREIGN KEY (`IdMateria`) REFERENCES `materia` (`IdMateria`),
  ADD CONSTRAINT `ciclomateria_ibfk_7` FOREIGN KEY (`Id_Rol`) REFERENCES `rol` (`IdRol`),
  ADD CONSTRAINT `ciclomateria_ibfk_8` FOREIGN KEY (`IdClase`) REFERENCES `clase` (`IdClase`);

--
-- Filtros para la tabla `clase`
--
ALTER TABLE `clase`
  ADD CONSTRAINT `clase_ibfk_1` FOREIGN KEY (`IdAnuncio`) REFERENCES `anuncio` (`IdAnuncio`),
  ADD CONSTRAINT `clase_ibfk_2` FOREIGN KEY (`Id_CicloMateria`) REFERENCES `ciclomateria` (`IdCicloMateria`),
  ADD CONSTRAINT `clase_ibfk_3` FOREIGN KEY (`IdTema`) REFERENCES `tema` (`IdTema`),
  ADD CONSTRAINT `clase_ibfk_4` FOREIGN KEY (`IdRol`) REFERENCES `rol` (`IdRol`),
  ADD CONSTRAINT `clase_ibfk_5` FOREIGN KEY (`IdActividad`) REFERENCES `actividad` (`IdActividad`),
  ADD CONSTRAINT `clase_ibfk_6` FOREIGN KEY (`IdRegistro`) REFERENCES `registro` (`IdRegistro`);

--
-- Filtros para la tabla `dni`
--
ALTER TABLE `dni`
  ADD CONSTRAINT `dni_ibfk_1` FOREIGN KEY (`IdTipoDni`) REFERENCES `tipodni` (`IdTipoDni`);

--
-- Filtros para la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD CONSTRAINT `domicilio_ibfk_1` FOREIGN KEY (`IdLocalidad`) REFERENCES `localidad` (`IdLocalidad`);

--
-- Filtros para la tabla `entrecalles`
--
ALTER TABLE `entrecalles`
  ADD CONSTRAINT `entrecalles_ibfk_1` FOREIGN KEY (`IdDomicilio`) REFERENCES `domicilio` (`IdDomicilio`);

--
-- Filtros para la tabla `institucion`
--
ALTER TABLE `institucion`
  ADD CONSTRAINT `institucion_ibfk_1` FOREIGN KEY (`IdTelefono`) REFERENCES `telefono` (`IdTelefono`),
  ADD CONSTRAINT `institucion_ibfk_2` FOREIGN KEY (`IdNivel`) REFERENCES `nivel` (`IdNivel`),
  ADD CONSTRAINT `institucion_ibfk_3` FOREIGN KEY (`IdDomicilio`) REFERENCES `domicilio` (`IdDomicilio`);

--
-- Filtros para la tabla `localidad`
--
ALTER TABLE `localidad`
  ADD CONSTRAINT `localidad_ibfk_1` FOREIGN KEY (`IdCodigoPostal`) REFERENCES `codigopostal` (`IdCodigoPostal`),
  ADD CONSTRAINT `localidad_ibfk_2` FOREIGN KEY (`IdPartido`) REFERENCES `partido` (`IdPartido`);

--
-- Filtros para la tabla `nivel`
--
ALTER TABLE `nivel`
  ADD CONSTRAINT `nivel_ibfk_1` FOREIGN KEY (`IdEspecialidad`) REFERENCES `especialidad` (`IdEspecialidad`);

--
-- Filtros para la tabla `partido`
--
ALTER TABLE `partido`
  ADD CONSTRAINT `partido_ibfk_1` FOREIGN KEY (`IdProvincia`) REFERENCES `provincia` (`IdProvincia`);

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`IdTelefono`) REFERENCES `telefono` (`IdTelefono`),
  ADD CONSTRAINT `persona_ibfk_2` FOREIGN KEY (`IdDomicilio`) REFERENCES `domicilio` (`IdDomicilio`),
  ADD CONSTRAINT `persona_ibfk_3` FOREIGN KEY (`IdGenero`) REFERENCES `genero` (`IdGenero`),
  ADD CONSTRAINT `persona_ibfk_4` FOREIGN KEY (`IdDni`) REFERENCES `dni` (`IdDni`);

--
-- Filtros para la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD CONSTRAINT `provincia_ibfk_1` FOREIGN KEY (`IdPais`) REFERENCES `pais` (`IdPais`);

--
-- Filtros para la tabla `registro`
--
ALTER TABLE `registro`
  ADD CONSTRAINT `registro_ibfk_1` FOREIGN KEY (`IdEstadoAlumno`) REFERENCES `estadoalumno` (`IdEstadoAlumno`),
  ADD CONSTRAINT `registro_ibfk_2` FOREIGN KEY (`IdRol`) REFERENCES `rol` (`IdRol`);

--
-- Filtros para la tabla `rol`
--
ALTER TABLE `rol`
  ADD CONSTRAINT `rol_ibfk_1` FOREIGN KEY (`IdPersona`) REFERENCES `persona` (`IdPersona`);

--
-- Filtros para la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD CONSTRAINT `telefono_ibfk_1` FOREIGN KEY (`IdTipoTelefono`) REFERENCES `tipotelefono` (`IdTipoTelefono`);

--
-- Filtros para la tabla `tema`
--
ALTER TABLE `tema`
  ADD CONSTRAINT `tema_ibfk_1` FOREIGN KEY (`IdClase`) REFERENCES `clase` (`IdClase`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
